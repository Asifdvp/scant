import React from "react";
import CustomModal from "./components/customModal";

import Scanner from "./Scanner";

function App() {
  const [open, setOpen] = React.useState(false);
  const [result, setResult] = React.useState("No result");
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const handleScan = (result: any) => {
    if (result) {
      console.log(result);

       setResult(result.data);
       handleClose()
    }
  };

  const handleError = (error: any) => {
    console.log(error);
  };
  return (
    <div className="app">
      <CustomModal
        open={open}
        handleClose={handleClose}
        handleOpen={handleOpen}
      >
        <Scanner
          handleScan={handleScan}
          result={result}
          handleError={handleError}
        />
      </CustomModal>
      <div>{result}</div>
    </div>
  );
}

export default App;

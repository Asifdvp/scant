import React, { useState } from "react";
import QrReader from "react-web-qr-reader";

const Scanner = ({ result, handleScan, handleError }: any) => {
  const delay = 2000;

  return (
    <div>
      <QrReader delay={delay} onError={handleError} onScan={handleScan} />
    
    </div>
  );
};

export default Scanner;
